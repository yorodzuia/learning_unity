﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 2.5f;
    public float Speed 
    {
        get { return speed; }
        set { speed = value; }
    }
    [SerializeField] private float force;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private float minimalHaight;
    [SerializeField] private bool isCheatMode;
    [SerializeField] private GroundDetection groundDetection;
    [SerializeField] private Vector3 direction;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float shootForce = 5;
    [SerializeField] private Arrow arrow;
    [SerializeField] private float cooldown;
    [SerializeField] private int arrowCount = 3;
    private List<Arrow> arrowPool;
    private Arrow currentArrow;
    private bool isCooldown;
    private bool isJumping;


    private void Start()
    {
        arrowPool = new List<Arrow>();
        for (int i = 0; i < arrowCount; i++)
        { 
        var arrowTemp = Instantiate (arrow, arrowSpawnPoint);
            arrowPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
        }
    }
    void FixedUpdate()
    {
        animator.SetBool("isGrounded", groundDetection.isGrounded);
        if (!isJumping && !groundDetection.isGrounded)
        {
            animator.SetTrigger("SrartFall");
        }
        isJumping = isJumping && !groundDetection.isGrounded;
        direction = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left;
        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right;
        direction *= speed;
        direction.y = rigidbody.velocity.y;
        rigidbody.velocity = direction;

        if (Input.GetKeyDown(KeyCode.Space) && groundDetection.isGrounded)
        {
            rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
        }
           

        if (direction.x > 0)
            spriteRenderer.flipX = false;
        if (direction.x < 0)
            spriteRenderer.flipX = true;

        animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
        CheckFall();    
    }

    private void Update()
    {
        CheckShoot();
    }
    void CheckShoot()
    {
        if (Input.GetMouseButtonDown(0) && !isCooldown)
        {
            animator.SetTrigger("StartShoot");
            //GameObject prefab = Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);
            //prefab.GetComponent<Arrow>().SetImpulse
            //    (Vector2.right,spriteRenderer.flipX ? -force * shootForce : force * shootForce, gameObject);
        }
    }

    public void InitArrow() 
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse(Vector2.right,0,this);
        
    }

    private void Shoot()
    {
        currentArrow.SetImpulse(Vector2.right, 
            spriteRenderer.flipX ? -force * shootForce : force * shootForce, this);
        StartCoroutine(Cooldown());
    }

    private IEnumerator Cooldown()
    {
        isCooldown = true;
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
        yield break;
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        else
        return Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);
        
    }

    public void ReturArrowToPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
            arrowPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }
    void CheckFall()
    {
        if (transform.position.y < minimalHaight && isCheatMode)
        {
            rigidbody.velocity = new Vector2(x: 0, y: 0);
            transform.position = new Vector3(x: 0, y: 0, z: 0);
        }

        else if (transform.position.y < minimalHaight && !isCheatMode)
            Destroy(gameObject);
    }


}